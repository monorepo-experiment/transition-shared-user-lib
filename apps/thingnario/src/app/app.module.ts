import { FeatureVppModule } from '@transition-shared-user-lib/feature/vpp';
import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [FeatureVppModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
