import { Module } from '@nestjs/common';
import { FeatureEmsModule } from '@transition-shared-user-lib/feature/ems';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [FeatureEmsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
