import { Injectable } from '@nestjs/common';

@Injectable()
export class SharedUserService {
  validate(token: string) {
    return token === 'thingnario';
  }
}
