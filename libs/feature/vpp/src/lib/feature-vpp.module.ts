import { SharedUserModule } from '@transition-shared-user-lib/shared/user';
import { Module } from '@nestjs/common';
import { FeatureVppController } from './feature-vpp.controller';

@Module({
  imports: [SharedUserModule],
  controllers: [FeatureVppController],
  providers: [],
  exports: [],
})
export class FeatureVppModule {}
